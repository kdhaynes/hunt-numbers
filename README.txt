This project uses python via a Jupyter Notebook to compare convolutional
neural networks (CNN) to PDP-style neural networks on various MNIST tasks.

1. Original MNIST.
2. Random Digit Placement.
3. Train Top Half Digit Placement, Test Bottom Half Digit Placement.
4A. Random Rescale and Digit Placement.
4B. Random Flip.
4C. Random Flip and Digit Placement. 
4D. Random Degrade and Digit Placement.
4E. Random Warp and Digit Placement.

This uses the following python libraries:
tensorflow, tensorflow.keras, cv2, numpy, pandas, sklearn, matplotlib

Our report on these tasks is huntReport.pdf.

Katherine Haynes (Katherine.Haynes@colostate.edu)
April 16, 2020
CS 510 PA4